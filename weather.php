<?php
require('functions.php');
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json');

if (!in_array($_GET['yahooWoeid'], array(2295423, 2293962, 2295301, 12586465, 2295316, 2294475, 2295329, 2295334, 2294557, 2295351, 2294673, 2294690, 2295368, 2295426))) {
    exit('');
}

$cacheDir = 'caches/weather/';
$currentTimeStamp = time();
if(is_readable($cacheDir.$_GET['yahooWoeid'].'.json')){
    if ($rawFileContent = file_get_contents($cacheDir.$_GET['yahooWoeid'].'.json')) {
        $jsonDecodedFileContent = json_decode($rawFileContent);
        if (($currentTimeStamp - $jsonDecodedFileContent->cacheTime) > 3600) {
            $noCache = true;
        } else {
            header("Expires: ".gmdate("D, d M Y H:i:s", $jsonDecodedFileContent->cacheTime + 3600) . " GMT");
            header("Cache-Control: max-age=3600, must-revalidate"); 
            echo json_encode($jsonDecodedFileContent->data);
        }
    } else {
        $noCache = true;
    }
} else {
    $noCache = true;
}

if ($noCache) {
    $rawArray = xml2array('http://weather.yahooapis.com/forecastrss?w=' . $_GET['yahooWoeid'] . '&u=c');
    $parsedArray = array();
    $parsedArray['condition'] = $rawArray['rss']['channel']['item']['yweather:condition_attr'];
    $parsedArray['forecast'] = array(
        $rawArray['rss']['channel']['item']['yweather:forecast']['0_attr'],
        $rawArray['rss']['channel']['item']['yweather:forecast']['1_attr'],
        $rawArray['rss']['channel']['item']['yweather:forecast']['2_attr'],
        $rawArray['rss']['channel']['item']['yweather:forecast']['3_attr'],
        $rawArray['rss']['channel']['item']['yweather:forecast']['4_attr']
    );

    file_put_contents($cacheDir.$_GET['yahooWoeid'].'.json', json_encode(
        array(
            'cacheTime' => $currentTimeStamp,
            'data' => $parsedArray
        )
    ));

    header("Expires: ".gmdate("D, d M Y H:i:s", $currentTimeStamp + 3600) . " GMT");
    header("Cache-Control: max-age=3600, must-revalidate"); 
    echo json_encode($parsedArray);
}
?>