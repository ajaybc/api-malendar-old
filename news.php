<?php
require('functions.php');
header('Access-Control-Allow-Origin:*');
header('Content-Type:application/json');

if (!in_array($_GET['source'], array('manorama', 'mathrubhumi'))) {
    exit('');
}

$cacheDir = 'caches/news/';
$currentTimeStamp = time();
if(is_readable($cacheDir.$_GET['source'].'.json')){
    if ($rawFileContent = file_get_contents($cacheDir.$_GET['source'].'.json')) {
        $jsonDecodedFileContent = json_decode($rawFileContent);
        if (($currentTimeStamp - $jsonDecodedFileContent->cacheTime) > 3600) {
            $noCache = true;
        } else {
            header("Expires: ".gmdate("D, d M Y H:i:s", $jsonDecodedFileContent->cacheTime + 3600) . " GMT");
            header("Cache-Control: max-age=3600, must-revalidate"); 
            echo json_encode($jsonDecodedFileContent->data);
        }
    } else {
        $noCache = true;
    }
} else {
    $noCache = true;
}

if ($noCache) {
    if ($_GET['source'] == 'manorama') {
        $url = 'http://www.manoramaonline.com/news/just-in.feed';
        $rawArray = xml2array($url);
        $parsedArray = array();
        if ($rawArray['feed']['entry']) {
            $i = 1;
            foreach ($rawArray['feed']['entry'] as $newsItem) {
                $parsedArray['news'][] = array(
                    'title' => $newsItem['title'],
                    'description' => preg_replace(
                        "/<\/?(a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|bgsound|big|blink|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|data|datalist|dd|del|details|dfn|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hgroup|hr|html|i|iframe|img|input|ins|isindex|kbd|keygen|label|legend|li|link|listing|main|map|mark|marquee|menu|menuitem|meta|meter|nav|nobr|noframes|noscript|object|ol|optgroup|option|output|p|param|plaintext|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|spacer|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr|xmp)\b[^<>]*>/", 
                        "", 
                        $newsItem['summary']
                    ),
                    'link' => $newsItem['link_attr']['href']
                );
                $i++;
                if ($i > 15) {
                    break;
                }
            }
        }
    } else {
        $url = 'http://feeds.feedburner.com/mathrubhumi';
        $rawArray = xml2array($url);
        $parsedArray = array();
        if ($rawArray['rss']['channel']['item']) {
            $i = 1;
            foreach ($rawArray['rss']['channel']['item'] as $newsItem) {
                $parsedArray['news'][] = array(
                    'title' => $newsItem['title'],
                    'description' => preg_replace(
                        "/<\/?(a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|bgsound|big|blink|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|data|datalist|dd|del|details|dfn|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hgroup|hr|html|i|iframe|img|input|ins|isindex|kbd|keygen|label|legend|li|link|listing|main|map|mark|marquee|menu|menuitem|meta|meter|nav|nobr|noframes|noscript|object|ol|optgroup|option|output|p|param|plaintext|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|spacer|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr|xmp)\b[^<>]*>/", 
                        "", 
                        $newsItem['description']
                    ),
                    'link' => $newsItem['link']
                );
                $i++;
                if ($i > 15) {
                    break;
                }
            }
        }
    }

    file_put_contents($cacheDir.$_GET['source'].'.json', json_encode(
        array(
            'cacheTime' => $currentTimeStamp,
            'data' => $parsedArray
        )
    ));

    header("Expires: ".gmdate("D, d M Y H:i:s", $currentTimeStamp + 3600) . " GMT");
    header("Cache-Control: max-age=3600, must-revalidate"); 
    echo json_encode($parsedArray);
}
?>